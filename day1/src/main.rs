use std::env;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;
use std::process;
use std::collections::HashSet;

fn main() {
  let args: Vec<String> = env::args().collect();
  if args.len() != 2 {
    println!("Wrong number of arguments. Provide just a file name.");
    process::exit(1);
  }
  let filename = &args[1];
  println!("Using file {} as input.", filename);
  let f = match File::open(filename) {
    Ok(file) => file,
    Err(e) => {
      println!("Failed to open file {}. {:?}", filename, e);
      process::exit(1);
    }
  };
  let file = BufReader::new(&f);

  let mut diffs: Vec<i32> = Vec::new();
  // exercise 1
  for line in file.lines() {
    let l = line.unwrap();
    let res = l.parse::<i32>();
    match res {
      Ok(value) => {
        diffs.push(value);
      }
      Err(e) => {
        println!("{:?}. l: {:?}", e, l);
        process::exit(1);
      }
    }
  }

  let result: i32 = diffs.iter().fold(0, |acc, x| acc + x);
  println!("Exercise 1: {}", result);

  // exercise 2
  let mut found = HashSet::new();
  found.insert(0);
  let mut sum = 0;
  loop {
    for diff in &diffs {
      sum = sum + diff;
      if found.contains(&sum) {
        println!("Exercise 2: {}", &sum);
        process::exit(0);
      }
      found.insert(sum);
    }
  }
}
