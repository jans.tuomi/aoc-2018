static INPUT: usize = 01245;
static INPUT_STR: &str = "293801";

fn visualize(scoreboard: &Vec<u32>, i1: usize, i2: usize) {
  for (i, val) in scoreboard.iter().enumerate() {
    match i {
      i if i == i1 => print!("({})", val),
      i if i == i2 => print!("[{}]", val),
      _ => print!(" {} ", val)
    }
  }
  print!("\n");
}

fn exercise1() {
  let mut scoreboard: Vec<u32> = vec![3, 7];
  let mut i1: usize = 0;
  let mut i2: usize = 1;

  for iter in 0..INPUT + 10 {
    // visualize(&scoreboard, i1, i2);
    let new_sum = scoreboard[i1] + scoreboard[i2];
    let string = new_sum.to_string();
    for ch in string.chars() {
      let s = ch.to_string();
      let score = s.parse::<u32>().unwrap();
      scoreboard.push(score);
    }

    let move1 = (scoreboard[i1] + 1) as usize;
    let move2 = (scoreboard[i2] + 1) as usize;

    let n = scoreboard.len();
    i1 = (i1 + move1) % n;
    i2 = (i2 + move2) % n;
  }

  print!("Result: ");
  for vi in INPUT..INPUT + 10 {
    let v = scoreboard[vi];
    print!("{}", v);
  }
  print!("\n");
}

fn exercise2() {
  let mut scoreboard: Vec<u32> = vec![3, 7];
  let mut i1: usize = 0;
  let mut i2: usize = 1;

  let input_len = INPUT_STR.len();
  let ITER_MAX = 100000000;
  for _ in 0..ITER_MAX {
    // visualize(&scoreboard, i1, i2);
    let new_sum = scoreboard[i1] + scoreboard[i2];
    let string = new_sum.to_string();
    for ch in string.chars() {
      let s = ch.to_string();
      let score = s.parse::<u32>().unwrap();
      scoreboard.push(score);

      let n = scoreboard.len();
      if n >= input_len {
        let slice: String = scoreboard[n - input_len..].iter().map(|&v| v.to_string()).collect();
        if slice == INPUT_STR {
          println!("Result 2: {}", n - input_len);
          return;
        }
      }
    }

    let move1 = (scoreboard[i1] + 1) as usize;
    let move2 = (scoreboard[i2] + 1) as usize;

    let n = scoreboard.len();
    i1 = (i1 + move1) % n;
    i2 = (i2 + move2) % n;
  }
  println!("Iteration max");
}

fn main() {
  // exercise1();
  exercise2();
}
