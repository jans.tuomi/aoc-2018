use std::env;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;
use std::process;
use std::collections::HashSet;

#[macro_use] extern crate text_io;

mod topo_order;
mod schedule;

type Vertex = char;
type Edge = (Vertex, Vertex);

fn main() {
  let args: Vec<String> = env::args().collect();
  if args.len() != 3 {
    println!("Wrong number of arguments. Provide a file name and worker count.");
    process::exit(1);
  }
  let filename = &args[1];
  let worker_count: u32 = args[2].parse().expect("Cannot parse worker count.");
  println!("Using file {} as input.", filename);
  let f = match File::open(filename) {
    Ok(file) => file,
    Err(e) => {
      println!("Failed to open file {}. {:?}", filename, e);
      process::exit(1);
    }
  };
  let file = BufReader::new(&f);
  let lines: Vec<String> = file.lines()
    .map(|line| line.expect("Could not parse line."))
    .collect();

  let mut V: HashSet<Vertex> = HashSet::new();
  let mut E: HashSet<Edge> = HashSet::new();
  let mut V_sources: HashSet<Vertex> = HashSet::new();
  let mut V_sinks: HashSet<Vertex> = HashSet::new();
  for line in &lines {
    let (s1, s2): (String, String);
    scan!(line.bytes() => "Step {} must be finished before step {} can begin.", s1, s2);
    assert_eq!(s1.len(), 1);
    assert_eq!(s2.len(), 1);
    let ch1: Vec<char> = s1.chars().collect();
    let ch2: Vec<char> = s2.chars().collect();
    let v1 = ch1[0] as Vertex;
    let v2 = ch2[0] as Vertex;
    let e = (v1, v2) as Edge;

    V.insert(v1);
    V.insert(v2);
    E.insert(e);
  }

  for v in &V {
    let edges_where_source: Vec<&Edge> = E.iter()
      .filter(|&(v1, _v2)| *v1 == *v)
      .collect();
    if edges_where_source.len() == 0 {
      V_sinks.insert(*v);
    }

    let edges_where_sink: Vec<&Edge> = E.iter()
      .filter(|&(_v1, v2)| *v2 == *v)
      .collect();
    if edges_where_sink.len() == 0 {
      V_sources.insert(*v);
    }
  }

  println!("V_sources len: {}", V_sources.len());
  println!("V_sinks len: {}", V_sinks.len());
  println!("V len: {}", V.len());
  println!("E len: {}", E.len());

  let result_1 = topo_order::find(&V, &E, &V_sources);
  println!("1. Result: {}", result_1);

  let result_2 = schedule::find(&V, &E, &V_sources, worker_count);
  println!("2. Result: {}", result_2);
}